package ru.tsc.goloshchapov.tm.exception.empty;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("Exception! Domain is empty!");
    }

}
