package ru.tsc.goloshchapov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.data.BackupLoadCommand;
import ru.tsc.goloshchapov.tm.command.data.BackupSaveCommand;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 30000;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.runCommand(BackupLoadCommand.NAME);
    }

    public void save() {
        bootstrap.runCommand(BackupSaveCommand.NAME);
    }

}
