package ru.tsc.goloshchapov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.goloshchapov.tm.api.repository.*;
import ru.tsc.goloshchapov.tm.api.service.*;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.command.data.BackupLoadCommand;
import ru.tsc.goloshchapov.tm.command.data.BackupSaveCommand;
import ru.tsc.goloshchapov.tm.comparator.ComparatorCommandByName;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.entity.CommandNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.repository.*;
import ru.tsc.goloshchapov.tm.service.*;
import ru.tsc.goloshchapov.tm.util.SystemUtil;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectTaskService, projectRepository);

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, authRepository, propertyService);

    @Getter
    @NotNull
    private final ILogService logService = new LogService();

    @Getter
    @NotNull
    private final Backup backup = new Backup(this);

    public void start(@Nullable String[] args) {
        initAppData();
        System.out.println("\n** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);

        @Nullable String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("\nENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
            } catch (final Exception exception) {
                logService.error(exception);
            }
        }
    }

    private void initAppData() {
        initPID();
        initCommands();
        initData();
        backup.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Gamma", "-", Status.COMPLETED));
        projectService.add(userService.findByLogin("admin").getId(), new Project("Project Alpha", "-"));
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Beta", "-", Status.IN_PROGRESS));
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Delta", "-", Status.COMPLETED));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Gamma", "-", Status.COMPLETED));
        taskService.add(userService.findByLogin("admin").getId(), new Task("Task Alpha", "-"));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Beta", "-", Status.IN_PROGRESS));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Delta", "-", Status.COMPLETED));
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.goloshchapov.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.goloshchapov.tm.command.AbstractCommand.class)
                .stream()
                .sorted(ComparatorCommandByName.getInstance())
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new CommandNotFoundException(args[0]);
        command.execute();
        return true;
    }

    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new CommandNotFoundException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotFoundException(command);
        if (!abstractCommand.toBriefString().equals(BackupLoadCommand.NAME) && !abstractCommand.toBriefString().equals(BackupSaveCommand.NAME)) {
            authService.checkRoles(abstractCommand.roles());
        }
        abstractCommand.execute();
    }

}
