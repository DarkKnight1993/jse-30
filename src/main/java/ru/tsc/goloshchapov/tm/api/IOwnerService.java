package ru.tsc.goloshchapov.tm.api;

import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
}
