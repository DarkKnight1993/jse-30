package ru.tsc.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class VersionShowCommand extends AbstractSystemCommand {
    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
        System.out.println(serviceLocator.getPropertyService().getBuildNumber());
    }

}
