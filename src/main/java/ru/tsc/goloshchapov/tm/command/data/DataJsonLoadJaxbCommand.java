package ru.tsc.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.dto.Domain;
import ru.tsc.goloshchapov.tm.exception.system.FileIsNotExistException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonLoadJaxbCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "data-load-json-jaxb";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return "Load data in json format by jaxb";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD BY JAXB]");
        System.setProperty(SYSTEM_JSON_PROPERTY_NAME, SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        if (!file.exists()) throw new FileIsNotExistException(FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(JAXB_JSON_PROPERTY_NAME, JAXB_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
        System.out.println("[OK]");
    }

}
