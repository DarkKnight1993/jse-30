package ru.tsc.goloshchapov.tm.command.projecttask;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectTaskShowByIdCommand extends AbstractProjectTaskCommand {
    @NotNull
    @Override
    public String name() {
        return "project-task-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all task of project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW ALL TASK OF PROJECT BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(userId, projectId);
        if (tasks == null) return;
        System.out.println("[TASK LIST FOR PROJECT WITH ID: " + projectId + " ]");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ") " + task.toString());
            index++;
        }
        System.out.println("[END LIST]");
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
